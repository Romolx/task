<?php

class siteController extends Controller{

   public function __construct(){
      $this->view = new View();
      $this->model = new siteModel();
   }

	public function action_index()
	{
      if(isset($_GET['page']))      
         $this_page = $_GET['page'];       
       else
         $this_page = 1;
      
      $show_pages = 3;

	   $this->view->render("index",$this->model->get_data($this_page,$show_pages));
	}

    public function action_addtask(){

        
        if(isset($_POST['text'])){
            $text = $_POST['text'];   
            $username = $_POST['username'];
            $email=$_POST['email'];
            $file_name=basename($_FILES["mini_file"]["tmp_name"]).$_FILES["pic"]["name"];        
            $upl_dir= PATH_APP.'/uploads/'.$file_name;
            
            if(is_uploaded_file($_FILES["mini_file"]["tmp_name"]))
            {	
                move_uploaded_file($_FILES["mini_file"]["tmp_name"],$upl_dir);            
                $this->model->set_data($text,'/uploads/'.$file_name,$username,$email);          
                echo '0';
                return;

            } else {
                echo '1';         
                return;
                }

            }
        $this->view->render("form_add");
    }

    public function action_edit(){
        
        if(isset($_POST['compl_val'])){
            $id = explode("l",$_POST['compl_key']);
            $val = $_POST['compl_val'];
            if($this->model->update_compl($id[1],$val));
            echo '1';            
        }
        if(isset($_POST['text_val'])){
            $id_t = explode("t",$_POST['text_key']);
            $val_t = $_POST['text_val'];
            if($this->model->update_text($id_t[3],$val_t));
            echo '1';
        }
        
        return ;
    }

	public function action_error404(){

	    $this->view->render("error404");
	}

}