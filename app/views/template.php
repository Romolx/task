
<!DOCTYPE html>
<html>
	<head>


		<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	        <link rel = "stylesheet" href= /css/style.css >
		<script src="/js/jquery-2.1.4.js" type="text/javascript"></script>		
		<script src="/js/script.js" type="text/javascript"></script>		
		<title>Задачи</title>
	</head>
	<body>

<div class="pure-menu pure-menu-horizontal" style="height:50px;">
    <ul >
        <li class="pure-menu-item"><a class="pure-menu-link" href="/" >Главная</a></li>
        <li class="pure-menu-item"><a class="pure-menu-link" href="/site/addtask" >Добавить задачу</a></li>
        <? global $is_admin; if($is_admin == true){?>
        <li class="pure-menu-item"><a class="pure-menu-link" href="/admin/logout" >Выход</a></li>
        <?}else{?>
        <li class="pure-menu-item"><a class="pure-menu-link" href="/admin" >Вход</a></li>
        <?}?>
   </ul>   
      
</div>

<? include PATH_APP."/app/views/".$content.'.php'; ?>

</body>

</html>