﻿<?php
class View
{	
	public $data;
	public function render($content, $data = null,$template = 'template')
	{
		$this->data = $data;
		include PATH_APP.'/app/views/'.$template.'.php';
	}
}