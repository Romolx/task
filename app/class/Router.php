﻿<?php
class Router{

	static function start(){

	$controller_name = 'site';
	$action_name = 'index';

	$route = explode('/',REQUEST);

	if(!empty($route[1])) {
			$tmp=explode('?',$route[1]);
			$controller_name = $tmp[0];
			}

	if(!empty($route[2])) {
			$tmp=explode('?',$route[2]);
			$action_name = $tmp[0];
			}
	$model_name = strtolower($controller_name)."Model";
	$controller_name = strtolower($controller_name)."Controller";
	$action_name = "action_".strtolower($action_name);
	$model_path = PATH_APP."/app/models/".$model_name.".php";

	if (file_exists($model_path))
		{
		include $model_path;
		}

	$controller_path = PATH_APP."/app/controllers/".$controller_name.".php";
	if (file_exists($controller_path))
		{
		include $controller_path;
		}
	else{
		header('Location:/site/error404');
	}

	$controller = new $controller_name;
	
	if(method_exists($controller,$action_name)){
		$controller->$action_name();
	}else{
		header('Location:/site/error404');
	}
	
	
	}


}