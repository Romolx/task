var pic_min;

$(document).ready(function(){

$('th').on('click',function(){
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc){rows = rows.reverse()}
    for (var i = 0; i < rows.length; i++){table.append(rows[i])}
})

$('select').on('change',function(e){
var msg ={"compl_val":$(this).val(),"compl_key":$(this).attr('name')};
   $.ajax({
      type: 'POST',
      url: '/site/edit',
      data: msg,
      success: function(ret) {		
      if(ret!=1)		
            alert('update error');
      },
      error:  function(xhr, str){
      alert('error : ' + xhr.responseCode);
      }
   });
});

$('#pic').on('change',function(e){

   $.each($('#pic')[0].files, function(i, file) {				
      var imageType = /image.*/;
      if (!file.type.match(imageType)) {
         alert('Загружать можно только изображения(png,jpg,gif)');
         return true;
      }
      picResized(file,320,240,'image/png');
      })

});

$('#load_task').on('click', function(e) {      

   var form = new FormData($('#uploadForm')[0]);			
   form.append('mini_file', pic_min);
   $.ajax({
      type: 'POST',
      url: '/site/addtask',
      data: form,
      cache: false,
      contentType: false,
      processData: false,
      success: function(ret){
         if(ret==0){
            $('#load_ok').fadeIn(2000);
            $('#load_ok').fadeOut(2000);
         }else{
            $('#load_no').fadeIn(2000);
            $('#load_no').fadeOut(2000);
         }           
            $('#uploadForm').each(function(){
                this.reset();
            }); 
      },
      error:  function(xhr, str){
      alert('error : ' + xhr.responseCode);
      }
   });  

});
$('#preview_close').on('click', function(e) { 
    $('#preview').children().remove();
    $('#preview_close').fadeOut(500);

});
$('#preview_btn').on('click', function(e) { 
    var el_tab = '<tr><td>0</td><td><div class="tsk"><img id="img_preview" src=#><p>'+$('#text').val()+'</p></div></td><td>'+$('#username').val()+'</td><td>'+$('#email').val()+'</td><td>Не выполнено</td></tr>';

    var tabl='<table class="pure-table pure-table-horizontal"><tr><th>Номер</th><th>Задача</th><th><span>Пользователь<span></th><th><span>Email<span></th><th><span>Статус<span></th></tr>'+el_tab+'</table>';
    $('#preview_close').fadeIn(500);
    $('#preview').append(tabl);
    var reader = new FileReader();  
    reader.onload = function(e) {$('#img_preview').attr('src', e.target.result);}
    reader.readAsDataURL(pic_min);

});

$('.tsk div').on('click', function(e) {

    var areaid = 'area'+$(this).attr('id');
    var sbtn = 'save'+$(this).attr('id');
    var editt = ' <textarea id="'+areaid+'" name = "text" style="width:300px; height:200px" >'+$(this).parent().find('p').text()+'</textarea>';
    var savebtn = '<button type="button"  id="'+sbtn+'" class="pure-button pure-button-primary">Сохранить</button>';
   
   $(this).fadeOut(500);
   $(this).parent().find('p').fadeOut(500);
   $(this).parent().append(editt);
   $(this).parent().append(savebtn);

   $(this).parent().find('button').on('click', function(ev) {

        var txt = $(this).parent().find('textarea').val();
        $(this).parent().find('p').text(txt);

        var msg ={"text_val":txt,"text_key":$(this).attr('id')};
        $.ajax({
            type: 'POST',
            url: '/site/edit',
            data: msg,
            success: function(ret) {
                if(ret!=1)		
            alert('update error');
            },
            error:  function(xhr, str){
            alert('error : ' + xhr.responseCode);
            }
        });
                
        $(this).parent().find('p').fadeIn(500);
        $(this).parent().find('div').fadeIn(500);

        $('#'+areaid).remove();
        $('#'+sbtn).remove();
   });

   
    
});

});


function comparer(index) {
    return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
    }
}
function getCellValue(row, index){ return $(row).children('td').eq(index).html() }
 

function dataURItoBlob(dataURI) {
  
  var byteString = atob(dataURI.split(',')[1]);  
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }  
  var blob = new Blob([ab], {type: mimeString});
  return blob;

}


function picResized(file, max_width, max_height,  imageEncoding){

    var fileLoader = new FileReader();
    canvas = document.createElement('canvas');
    context = null;
    imageObj = new Image();      

    canvas.id     = "hiddenCanvas";
    canvas.width  = max_width;
    canvas.height = max_height;
    canvas.style.visibility   = "hidden";   
    document.body.appendChild(canvas);  
    context = canvas.getContext('2d');  

    if (file.type.match('image.*')) {
        fileLoader.readAsDataURL(file);
    } else {
        alert('File is not an image');
    }

    fileLoader.onload = function() {
        var data = this.result; 
        imageObj.src = data;
    };

    fileLoader.onabort = function() {
        alert("The upload was aborted.");
    };

    fileLoader.onerror = function() {
        alert("An error occured while reading the file.");
    };  
    imageObj.onload = function() {  

        if(this.width == 0 || this.height == 0){
            alert('Image is empty');
        } else {                

            context.clearRect(0,0,max_width,max_height);
            context.drawImage(imageObj, 0, 0, this.width, this.height, 0, 0, max_width, max_height);
            pic_min = dataURItoBlob(canvas.toDataURL(imageEncoding));            
        }       
    };

    imageObj.onabort = function() {
        alert("Image load was aborted.");
    };

    imageObj.onerror = function() {
        alert("An error occured while loading image.");
    };

}

